const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}

function intrestedInVideoGame(users) {
    let arr = Object.entries(users);

    let result = arr.filter((user) => {
        let arrayToString = String(user[1]['interests'])
            .includes('Video Games');

        return arrayToString === true;
    });

    return Object.fromEntries(result);

}



function inGermany(users) {

    let arr = Object.entries(users);

    let result = arr.filter((user) => {
        return user[1]['nationality'] === "Germany";
    });

    return Object.fromEntries(result);

}



function sortUsers(users) {

    const usersInArray = Object.entries(users);

    const seniorDeveloper = usersInArray.filter((user) => {
        return user[1]['desgination'].includes('Senior');
    });

    const developer = usersInArray.filter((user) => {
        let developerPersent = user[1]['desgination'].includes('Developer') === true;
        let seniorNotPresent = user[1]['desgination'].includes('Senior') === false;

        return developerPersent && seniorNotPresent;
    });

    const intern = usersInArray.filter((user) => {
        return user[1]['desgination'].includes('Intern') === true;
    })
        .sort((user1, user2) => {
            return user2[1]['age'] - user1[1]['age'];
        })

    let concatAll = seniorDeveloper.concat(developer)
        .concat(intern);

    return Object.fromEntries(concatAll);

}

function masterDegree(users) {

    const arr = Object.entries(users);

    const result = arr.filter((user) => {
        return user[1]['qualification'].includes('Masters');
    });

    return Object.fromEntries(result);
}
